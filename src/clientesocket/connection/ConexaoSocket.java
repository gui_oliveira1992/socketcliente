
package clientesocket.connection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**Classe de conexão cliente socket
 * @author Guilherme
 * @version 1.00
 * @since Release 00 da aplicação
 */

public class ConexaoSocket {

    public String conectarSocket(String ip, int porta) {

        /**
         * Definindo objeto do tipo socket
         */
        Socket objConexao;
            
        /**
         *  Definindo a váriavel do tipo String
         */
        String mensagem = null;
        
        /**
         * Tratamento da exceção da conexão docket
         */
        try {
            /**
             * Instanciando o objemto do tipo Socket para receber a mensagem do servidor
             */
            objConexao = new Socket(ip, porta);
            
            /**
             * Instanciando o objeto responsável por receber a mensagem via Socket do servidor
             */
            ThreadRecebedor threadRecebedor = new ThreadRecebedor(objConexao);
            threadRecebedor.start();
            mensagem = threadRecebedor.executarRecebedor();

        } catch (Exception ex) {
            /**
             * Está mensagem será exibida caso ocorrer algum erro com a inicialização da comunicação socket com o servidor
             */ 
            JOptionPane.showMessageDialog(null, "Erro ao conectar com o servidor socket. Porta Padrão: 1050", "Erro", 2);
        }
        return mensagem;
    }
}
