package clientesocket.connection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**Classe de criação de thread e recebimento da mensagem via socket
 * @author Guilherme
 * @version 1.00
 * @since Release 00 da aplicação
 */

public class ThreadRecebedor extends Thread {

    private Socket recebedor;
    
  /**
  * Construtor da classe
  * 
  * @param recebedor   conexão do recebedor da mensagem socket
  * @author            Guilherme
  */
    public ThreadRecebedor(Socket recebedor) {
        this.recebedor = recebedor;
        
    }

  /**
  * Método que recebe as mensagens enviadas via socket
  * @param recebedor   conexão do recebedor da mensagem socket
  * @author            Guilherme
  */    
    public String executarRecebedor() {
        
        String mensagem = null;
        try {
            /**
             * Instanciando o objeto responsável por receber a mensagem via socket
             */
            ObjectInputStream entrada = new ObjectInputStream(recebedor.getInputStream());
            
            /**
             * Atribuindo a mensagem recebida do servidor para uma váriavel do tipo String
             */
            mensagem = entrada.readUTF();
            
            /**
             * Finalizando as instancias dos objetos da comunicação cliente socket
             */
            entrada.close();

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao receber a mensagem do servidor", "Erro", 2);
        }

        return mensagem;
    }
}
