package clientesocket;

import clientesocket.swing.FormTelaPrincipal;

/**Classe principal da aplicação cliente
 * @author Guilherme
 * @version 1.00
 * @since Release 00 da aplicação
 */
public class ClienteSocket {

    /**Método Principal da aplicação
     *
     * @param args recebe entrada de dados
     * @author Guilherme
     */
    public static void main(String[] args) {
        /**
         * Instanciando o objeto de interface com usuaário
         */
        FormTelaPrincipal formTelaPrincipal = new FormTelaPrincipal();
        
        /**
         * Chamada do método que abre a interface gráfica ao usuário da aplicação
         */
        formTelaPrincipal.show(true);
    }

}
